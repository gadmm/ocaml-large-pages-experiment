(* original benchmark by stedolan. Adapted for 32-bit compatibility
   and better suitability to my experiment. *)

(* multipier
   64-bit: 1 (800MB) 4 (3.2GB) 8 (6.4GB) 14 (11GB)
   32-bit: 1 (400MB) 2 (800MB)
*)
let multiplier = 1

type t =
    Orig of {
      mutable a : int option;
      mutable b : float;
      mutable c : float;
      mutable d : float;
      mutable e : float;
      mutable f : float;
    }
  | Nostatic of {
      mutable a : int option;
      mutable b : int;
      mutable c : int;
      mutable d : int;
      mutable e : int;
      mutable f : int;
    }
  | Obj of {
      mutable a : int option;
      mutable b : Obj.t;
      mutable c : Obj.t;
      mutable d : Obj.t;
      mutable e : Obj.t;
      mutable f : Obj.t;
    }

let word_size = Sys.word_size / 8

let () = Printf.printf "Major heap target: ~%n MB\n" (100 * word_size * multiplier)

(* let () = Gc.set { (Gc.get()) with Gc.minor_heap_size = 524288 } (* 4MB *) *)

let () = Printf.printf "Minor heap size: %n B\n"
           (Gc.(get()).minor_heap_size * word_size)

let () = Gc.set { (Gc.get()) with Gc.verbose = 0x8 + 0x4 }

let () = Random.init 4048551289618392954

let make_elem_orig nums i =
  Orig { a = nums.(i land 0xfffff); b = 0.; c = 0.; d = 0.; e = 0.; f = 42. }

let make_elem_nostatic nums i =
  Nostatic { a = nums.(i land 0xfffff); b = 0; c = 0; d = 0; e = 0; f = 42 }

let max_string = 3000

(* main test used for the benchmarks *)
let make_elem_strings static_every_n nums i =
  let block = nums.(i land 0xfffff) in
  let imm_or_static () =
    if i mod static_every_n = 0
    then Obj.repr (Static_strings.string_of_index (Random.int max_string))
    else Obj.repr 1
  in
  Obj { a = block;
        b = imm_or_static () ;
        c = imm_or_static () ;
        d = imm_or_static () ;
        e = imm_or_static () ;
        f = imm_or_static () }

let make_elem_random static_every_n pc_immediates nums i =
  let block = nums.(i land 0xfffff) in
  let block_or_static () =
    if i mod static_every_n = 0
    then Obj.repr (Static_strings.string_of_index (Random.int max_string))
    else Obj.repr block
  in
  let rand_imm_or_block () =
    if pc_immediates > Random.int 100 then Obj.repr (Random.int 100000000)
    else block_or_static ()
  in
  Obj { a = block;
        b = rand_imm_or_block () ;
        c = rand_imm_or_block () ;
        d = rand_imm_or_block () ;
        e = rand_imm_or_block () ;
        f = rand_imm_or_block () }

let make_arr make_elem =
  let nums = Array.init (1 lsl 20) (fun i -> Some i) in
  let length = multiplier * 1024 in
  let a = Array.init length (fun _ -> []) in
  for i = 1 to multiplier * 10_000_000 do
    let n = Random.int length in
    a.(n) <- (make_elem nums i) :: a.(n);
  done;
  a

let make_arr_norandom make_elem =
  let nums = Array.init (1 lsl 20) (fun i -> Some i) in
  let length = 256 in
  let a = Array.init length (fun _ -> []) in
  let max = multiplier * 10_000_000 in
  for i = 1 to max do
    let n = (i / (max / length)) mod length in
    a.(n) <- (make_elem nums i) :: a.(n);
  done;
  a

(* skip ratio:
   1: 62.5%
   2: 45.4%
   7: 19.2%
   15: 10.0%
   32: 5.0%
   80: 2.0%
   160: 1.0%
   1000000000: 0.0% *)
let static_every_n = 32

let static_every_n_of_skip_ratio = function
  | 0 -> 1000000000
  | 1 -> 160
  | 2 -> 80
  | 5 -> 32
  | 10 -> 15
  | 20 -> 7
  | 40 -> 2
  | _ -> raise (Invalid_argument "allowable skip ratios: 0 1 2 5 10 20 40")

let arr, old =
  let make_arr, make_elem, title, old =
    match Sys.argv with
    | [| _; "nostatic" |] ->
        (make_arr,
         make_elem_nostatic,
         "No static data (0% skip ratio)",
         false)
    | [| _; "norandom"; i |] ->
        let ratio = (int_of_string i) in
        let n = static_every_n_of_skip_ratio ratio in
        (make_arr_norandom,
         make_elem_strings n,
         (Printf.sprintf "No heap randomness (%d%% skip ratio)" ratio),
         false)
    | [| _; "strings" |] ->
        (make_arr,
         make_elem_strings static_every_n,
         "Static strings (5% skip ratio)",
         false)
    | [| _; "strings"; i |] ->
        let ratio = (int_of_string i) in
        let n = static_every_n_of_skip_ratio ratio in
        (make_arr,
         make_elem_strings n,
         (Printf.sprintf "Static strings (%d%% skip ratio)" ratio),
         false)
    | [| _; "immediates" |] ->
        (make_arr,
         make_elem_random (static_every_n_of_skip_ratio 0) 50,
         "Random immediates (50%)",
         false)
    | [| _; "random"; i; j |] ->
        let ratio_s = (int_of_string i) in
        let pc_imm = (int_of_string j) in
        let n = static_every_n_of_skip_ratio ratio_s in
        (make_arr,
         make_elem_random n pc_imm,
         (Printf.sprintf "Random (%d%% skip ratio minus immediates %d%%)" ratio_s pc_imm),
         false)
    | [| _; "orig" |] ->
        (make_arr,
         make_elem_orig,
         "Original benchmark",
         true)
    | _ -> raise (Invalid_argument "invalid argument")
  in
  print_endline title;
  make_arr make_elem, old

let rec sum = function
    [] -> 0.
  | n :: l -> n +. sum l

let avg l =
  sum l /. (float_of_int (List.length l))

let deviation l =
  let square n = n *. n in
  let square_avg = avg (List.map square l) in
  let avg_square = square (avg l) in
  Float.sqrt (square_avg -. avg_square)

let rec list_min = function
  | [] -> 1000000.
  | n :: l -> min n (list_min l)

let () =
  (*let () = Gc.compact()*)
  let () = print_endline "setup done" in
  if old then (
    (* old test *)
    let n = 5 in
    let tstart = Unix.gettimeofday () in
    for i = 1 to n do
      Gc.full_major ();
    done;
    let tend = Unix.gettimeofday () in
    Printf.printf "%.3f s/gc\n" ((tend -. tstart) /. float_of_int n)
  ) else (
    (* new test *)
    let res = ref [] in
    let n = 20 in
    (* do a major GC to make sure there is no influence of the major
       GC state (in particular due to varying major heap chunk
       sizes). *)
    Gc.full_major ();
    for i = 1 to n do
      let tstart = Unix.gettimeofday () in
      Gc.major ();
      let tend = Unix.gettimeofday () in
      let t = (tend -. tstart) *. 1000. in
      Printf.printf "%.1f ms/gc\n" t ;
      res := t :: !res
    done;
    Printf.printf "%.1f ms/gc (best of 5)\n" (list_min (!res)) ;
    Printf.printf "%.1f ms/gc (avg +/- %.1f)\n" (avg !res) (deviation !res)
  );
  ignore (Sys.opaque_identity arr)

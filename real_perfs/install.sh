#!/bin/bash

# Config

#REPOS=`cat repo_list`
REPOS="mark-prefetching-nnp page-table-vas-pt-alt page-table-vas-pt page-table-vas-nnp is_not_young-nnp"
EPOCH=.54 # appended to the names of all log files
TRIALS=4
# compilation
USE_CLANG=false
USE_MARCH=false
USE_ALIGN=false
REINSTALL=true # must be true if compilation options are changed
# benchmarks
BENCH_REINSTALL=true
BENCH_GALLINA_DEV=false #obsolete
BENCH_GALLINA_OPAM=true
BENCH_MATHCOMP=true
BENCH_MATHCOMP_HEAVY=false

DRY= #--dry-run

# Configure your CPU to avoid scaling and throttling
#   cpufreqctl --governor --set=performance &&
#   cpufreqctl --max-perf --set=100 &&
#   cpufreqctl --min-perf --set=100 &&
#   cpufreqctl --no-turbo --set=1
# set J low enough
# make sure the IS_THROTTLED control works
J=2

# Internals

THROTTLE_CMD="cpufreqctl --throttle" # number of times the CPUs have
                                     # been throttled
TIME=time

GALLINA_C="coq coq-core coqide coqide-server coq-stdlib"

GALLINA_PKGS="coq-mathcomp-field coq-mathcomp-solvable
              coq-mathcomp-algebra coq-mathcomp-fingroup
              coq-mathcomp-ssreflect"

GALLINA_HEAVY_PKGS="coq-mathcomp-character coq-mathcomp-odd-order"

export OPAMVAR_ocamlas="as -mbranches-within-32B"

MARCH=""

if $USE_MARCH; then
    MARCH="-march=skylake -fgcse-after-reload -fipa-cp-clone -floop-interchange \
           -floop-unroll-and-jam -fpeel-loops -fpredictive-commoning \
           -fsplit-loops -fsplit-paths -ftree-loop-distribution \
           -ftree-loop-vectorize -ftree-partial-pre -ftree-slp-vectorize\
           -funswitch-loops -fvect-cost-model -fvect-cost-model=dynamic \
           -fversion-loops-for-strides" # gives a different code
                                        # layout, essentially
                                        # "-march=native -O3" plus
                                        # stricter function and loop
                                        # alignment (note that we
                                        # cannot specify -O3 via the
                                        # CC argument)
fi

if $USE_MARCH && $USE_CLANG; then
    MARCH="-march=skylake -fslp-vectorize"
fi

ALIGN=""

if $USE_CLANG; then
    if $USE_ALIGN; then ALIGN="-falign-functions=16"; fi
   export OPAMVAR_ocamlcc="clang -mbranches-within-32B-boundaries ${MARCH} ${ALIGN}"
else
    if $USE_ALIGN; then ALIGN="-falign-functions=16 -falign-loops=16 -falign-jumps=16"; fi
   export OPAMVAR_ocamlcc="gcc -Wa,-mbranches-within-32B ${MARCH} ${ALIGN}"
fi

unset OPAMSWITCH

throttle_control_begin(){
    IS_THROTTLED=
    THROTTLE=`$THROTTLE_CMD`
}

throttle_control_end(){
    if [ $THROTTLE -eq `$THROTTLE_CMD` ]
    then IS_THROTTLED=
    else IS_THROTTLED=-throttled
    fi
}

rm /tmp/ocaml-stats-*.log

# (Re)install ocaml-variants, assumed invalid as an experiment
for i in $REPOS; do
    cd repo-${i}
    eval $(opam env --switch=. --set-switch)
    opam list
    opam remove $DRY -y coq num zarith
    # fix opam/packaging bug (???)
    rm -rf $DRY _opam/lib/zarith
    rm -rf $DRY _opam/lib/stublibs/dllzarith.so*
    if $REINSTALL; then c=reinstall; else c=install; fi
    opam $c $DRY -y ocaml-variants zarith lablgtk3-sourceview3 dune || exit 1
    rm $DRY /tmp/ocaml-stats-${i}.log
    cd ..
done

for n in $(seq 0 $((TRIALS - 1))); do
    TRIAL=-${n}

    echo "Iteration ${n}"
    date -R

    if $BENCH_REINSTALL; then
        # Reinstall ocaml-variants, benchmarked
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            opam remove $DRY -y coq num zarith
            # fix opam/packaging bug (???)
            rm -rf $DRY _opam/lib/zarith
            rm -rf $DRY _opam/lib/stublibs/dllzarith.so*
            $TIME opam reinstall --skip-updates -j $J $DRY -y ocaml-variants zarith
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log reinstall-ocaml-variants.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

    if $BENCH_GALLINA_DEV; then
        # Compile Gallina from sources
        if [ -z $DRY ]; then # does NOT run if $DRY is non-empty
            cd coq
            echo "Gallina git commit:"
            git rev-parse HEAD
            for i in $REPOS; do
                throttle_control_begin
                eval $(opam env --switch=../repo-${i} --set-switch)
                make clean
                ./configure -native-compiler no -local
                $TIME make -j $J world
                throttle_control_end
                mv /tmp/ocaml-stats-${i}.log ../repo-${i}/coq-dev.log${EPOCH}${TRIAL}${IS_THROTTLED}
            done
            cd ..
        fi
        date -R
    fi

    if $BENCH_GALLINA_OPAM || $BENCH_MATHCOMP || $BENCH_MATHCOMP_HEAVY; then
        # Compile Gallina from github.com/gadmm using opam
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            # for p in $GALLINA_C; do
            #     opam pin -n -y $DRY $p https://github.com/gadmm/coq.git#nnp
            # done
            if $BENCH_GALLINA_OPAM; then
                opam remove $DRY -y $GALLINA_PKGS $GALLINA_HEAVY_PKGS
                c=reinstall
            else
                c=install
            fi
            $TIME opam $c --skip-updates $DRY -j $J -y $GALLINA_C
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log coq-opam.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

    if $BENCH_MATHCOMP; then
        # Compile a few Mathcomp packages
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            opam remove $DRY -y $GALLINA_HEAVY_PKGS
            $TIME opam reinstall --skip-updates $DRY -j $J -y $GALLINA_PKGS
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log coq-mathcomp.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

    if $BENCH_MATHCOMP_HEAVY; then
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            $TIME opam reinstall $DRY -j $J -y $GALLINA_HEAVY_PKGS
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log coq-mathcomp-heavy.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

done # TRIALS

unset OPAMSWITCH

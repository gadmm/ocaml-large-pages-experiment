git -C ../../.. checkout page-table-412-stats-alt && opam switch create . --empty && eval $(opam env --switch=.) && opam install -y ../../..
opam repo add coq-released https://coq.inria.fr/opam/released
GALLINA_C="coq coq-core coqide coqide-server coq-stdlib"
for p in $GALLINA_C; do
  opam pin -n -y $p https://github.com/gadmm/coq.git#nnp
done

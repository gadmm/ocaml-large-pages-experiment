.PHONY: makebench config-no-naked config-naked config-no-naked-32 config-naked-32 ocaml-clean runbench runperf runperfm runperfmlp runstrings

#JCC = CC='gcc -Wa,-mbranches-within-32B' AS='as -mbranches-within-32B'
#JCC = CC='clang -mbranches-within-32B-boundaries' AS='as -mbranches-within-32B'
JCC =
I32 = CC="gcc -m32 -march=x86-64 -Wa,-mbranches-within-32B" AS="as --32 -mbranches-within-32B" ASPP="gcc -m32 -c" --host i386-linux PARTIALLD="ld -r -melf_i386"
#MALLOC = MALLOC_CONF="thp:always" LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2
MALLOC =
PERF = perf stat -e \
cycles,cycles:u,instructions,instructions:u,cs,migrations,cpu-clock,minor-faults,major-faults,dTLB-loads,dTLB-load-misses,iTLB-loads #,dtlb_load_misses.stlb_hit,dtlb_load_misses.walk_pending,dtlb_load_misses.walk_active #,stalled-cycles-frontend,stalled-cycles-backend,branches,branch-misses,br_misp_retired.all_branches,int_misc.clear_resteer_cycles,int_misc.recovery_cycles,sw_prefetch_access.prefetchw,sw_prefetch_access.t0,sw_prefetch_access.t1_t2,sw_prefetch_access.nta
PERFM = perf stat -M Instructions,GFLOPS,CPI,IPC,TLB,Load_Miss_Real_Latency,MLP,Branch_Misprediction_Cost,IpMispredict
PERFMLP = perf stat -e \
cycles,l1d_pend_miss.pending_cycles,l1d_pend_miss.l2_stall,cycle_activity.stalls_l1d_miss,l1d_pend_miss.pending,l1d_pend_miss.fb_full,L1-dcache-loads
#HUGEPARAM = OCAMLRUNPARAM=H
HUGEPARAM =

ifeq ($(BENCH),)
#BENCH = orig
#BENCH = strings
#BENCH = norandom 0
BENCH = norandom 5
#BENCH = norandom 40
#BENCH = nostatic
#BENCH = immediates
#BENCH = strings 40
#BENCH = random 5 0 # 5% skip ratio, 0% immediates, random
#BENCH = random 5 20 # 5% skip ratio, 20% immediates, random
endif

runbench: makebench
	${MALLOC} ./bench ${BENCH}

runperf: makebench
	${MALLOC} ${PERF} ./bench ${BENCH}

runperfm: makebench
	${MALLOC} ${PERFM} ./bench ${BENCH}

runperfmlp: makebench
	${MALLOC} ${PERFMLP} ./bench ${BENCH}

runstrings: makebench
	sh ./bench_static.sh

ocaml:
	sh -c "cd .. && time make -j"

makebench: ocaml static_strings.cmx
	../runtime/ocamlrun ../ocamlopt -g -o bench -nostdlib -I ../stdlib -I ../otherlibs/unix -I ../otherlibs/systhreads -thread unix.cmxa threads.cmxa static_strings.cmx markbench.ml
	sh -c "cd .. && git rev-parse HEAD"
	grep "Target: " ../config.log
	grep "naked_pointers=" ../config.log

static_strings.cmx: static_strings.ml static_strings.mli
	../runtime/ocamlrun ../ocamlopt -g -nostdlib -I ../stdlib static_strings.mli
	../runtime/ocamlrun ../ocamlopt -g -nostdlib -I ../stdlib static_strings.ml

config-no-naked:
	cd .. && ./configure --disable-naked-pointers ${JCC}

config-naked:
	cd .. && ./configure ${JCC}

config-no-naked-32:
	cd .. && ./configure --disable-naked-pointers ${I32}

config-naked-32:
	cd .. && ./configure ${I32}

ocaml-clean:
	cd .. && make clean

#!/bin/sh
for FILE in "$@"
do
    echo "let x = ref []\n" > ${FILE}.ml
    grep "([*] OCaml" $FILE >> ${FILE}.ml
done

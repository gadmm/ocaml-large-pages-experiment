let n l t = Scanf.sscanf l "work_done=%d, duration(c)=%d, duration_inner(c)=%d %s" (fun w d di _ -> let w0, d0, di0 = !t in t:= (w0 + w, d0 + d, di0 + di))
let f = open_in (Sys.argv.(1))
let tot = ref (0,0,0)
let () =
  try
    while true do n (input_line f) (tot) done
  with _ -> ()

let () =
  let w,d,di = !tot in
  Printf.printf "work=%d, duration(c)=%d, duration_inner(c)=%d (%f%%)\n" w d di ((float_of_int di) *. 100. /. (float_of_int d))

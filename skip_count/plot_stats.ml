open! Owl
open Owl_plplot

type mat = Plot.dsmat

let () = Printexc.record_backtrace true

let vect_make = Mat.vector_zeros
let vect_set vec i n = Mat.set vec 0 i n

type data = { list: (float * float * float) list ;
              seen: mat ;
              skipped: mat ;
              cachelines: mat ;
              ratios: mat }

let data_of_list data_list =
  let length = List.length data_list in
  let float_list =
    let f = float_of_int in
    List.map (fun (c,s,l) -> f c, f s, f l) data_list
  in
  let data = { list = float_list ;
               seen = vect_make length ;
               skipped = vect_make length ;
               cachelines = vect_make length ;
               ratios = vect_make length } in
  List.iteri (fun i (c,s,l) ->
    vect_set data.seen i c ;
    vect_set data.skipped i s ;
    vect_set data.cachelines i l ;
    vect_set data.ratios i (if c = 0. then 0. else s /. c *. 100.) )
    float_list ;
  data

let plot_count_vs_skipped h data =
  Plot.set_xlabel h "Words seen (marked + skipped)" ;
  Plot.set_ylabel h "Words skipped" ;
  Plot.set_title h "Skipped vs. seen" ;
  Plot.(scatter ~h ~spec:[ RGB (128,0,0) ;
                           Marker "#[0x22b9]" ;
                           MarkerSize 3. ]
          data.seen
          data.skipped) ;
(* let avg = Mat.mean' data.ratios in *)
  let avg = (Mat.mean' data.skipped) /. (Mat.mean' data.seen) in
  let (min_x, max_x) = Mat.minmax' data.seen in
  Plot.plot_fun ~h ~spec:[ RGB (0,0,128) ] (fun i -> avg *. i) min_x max_x ;
  Plot.set_yrange h 0. (avg *. max_x);
  Plot.(legend_on h ~position:NorthWest
          [|Printf.sprintf "mark slices (%d)" (List.length data.list) ;
            Printf.sprintf "total skipped / seen = %.1f%%" (avg *. 100.)|]
       )

let filteri p l =
  let rec aux i acc = function
  | [] -> List.rev acc
  | x::l -> aux (i + 1) (if p i x then x::acc else acc) l
  in
  aux 0 [] l

let filter_mat mat f =
  let arr = Mat.to_array mat in
  let (min, max) = Mat.minmax' mat in
  let list = min :: max :: (filteri f (Array.to_list arr)) in
  let size = List.length list in
  Mat.of_array (Array.of_list list) 1 size

(* a fixed threshold makes sense, cache sizes are fixed *)
let cacheline_threshold = 128. (* 8KB *)

let filter_uniqueness cachelines mat =
  filter_mat mat (fun i _ -> Mat.get cachelines 0 i >= cacheline_threshold)

let plot_skip_ratios h data =
  Plot.set_xlabel h "Skip ratio (skipped/seen %)";
  Plot.set_ylabel h "Frequency" ;
  Plot.set_title h "Frequency of skip ratios" ;
  Plot.(histogram ~h ~spec:[ RGB (0,128,0) ] ~bin:60 data.ratios) ;
  Plot.(histogram ~h ~spec:[ RGB (0,0,192) ; FillPattern 3 ] ~bin:60 (filter_uniqueness data.cachelines data.ratios)) ;
  Plot.(legend_on h ~position:NorthEast
          [| "All" ;
             Printf.sprintf "Many unique cachelines (>=%d)" (int_of_float cacheline_threshold) |]
       )

let plot_skip_ratios_quartile3 h data =
  let quartile3, ratios_above, cachelines_above =
    let arr_ratios = Mat.to_array data.ratios in
    let arr_cachelines = Mat.to_array data.cachelines in
    let arr = Array.map2 (fun r c -> (r,c)) arr_ratios arr_cachelines in
    Array.sort (fun (r1,_) (r2,_) -> Float.compare r1 r2) arr ;
    let n = Array.length arr in
    assert (n > 0) ;
    let m = (3 * n) / 4 in
    let quartile3 = fst arr.(m) in
    let size = n - m in
    quartile3,
    Mat.init 1 size (fun i -> fst arr.(m+i)),
    Mat.init 1 size (fun i -> snd arr.(m+i))
  in
  Plot.set_xlabel h (Printf.sprintf "Skip ratio above 3rd quartile (%.1f%%)" quartile3) ;
  Plot.set_ylabel h "Frequency" ;
  Plot.set_title h "Frequency of skip ratios (3rd quartile)" ;
  Plot.(histogram ~h ~spec:[ RGB (0,128,0) ] ~bin:60 ratios_above) ;
  Plot.(histogram ~h ~spec:[ RGB (0,0,192) ; FillPattern 3 ] ~bin:60 (filter_uniqueness cachelines_above ratios_above)) ;
  Plot.(legend_on h ~position:NorthEast
          [| "All" ;
             Printf.sprintf "Many unique cachelines (≥%d)" (int_of_float cacheline_threshold) |]
       )

let plot_skip_ratios_vs_cachelines h data =
  Plot.set_xlabel h "Skip ratio (%)" ;
  Plot.set_ylabel h "## Cachelines" ;
  Plot.set_title h "Unique skipped cache lines per slice vs. skip ratios";
  Plot.(scatter ~h ~spec:[ RGB (0,128,128) ;
                           Marker "#[0x22b9]" ;
                           MarkerSize 3. ]
          data.ratios
          data.cachelines) ;
  Plot.(legend_on h ~position:NorthEast
          [|"mark slices"|])

let plot_seen_vs_skip h data =
  Plot.set_xlabel h "Seen per slice" ;
  Plot.set_ylabel h "Skip ratio (%)" ;
  Plot.set_title h "Skip ratio vs. seen per slice";
  Plot.(scatter ~h ~spec:[ RGB (128,0,128) ;
                           Marker "#[0x22b9]" ;
                           MarkerSize 3. ]
          data.seen
          data.ratios) ;
  Plot.(legend_on h ~position:NorthEast
          [|"mark slices"|])

let plot_unique_vs_skipped h data =
  Plot.set_xlabel h "Skipped words" ;
  Plot.set_ylabel h "Unique skipped cachelines (%)" ;
  Plot.set_title h "Unique skipped cachelines (%) vs. skipped words";
  Plot.(scatter ~h ~spec:[ RGB (192,96,0) ;
                           Marker "#[0x22b9]" ;
                           MarkerSize 3. ]
          data.skipped
          (Mat.scalar_mul 100. (Mat.div data.cachelines data.skipped))) ;
  Plot.(legend_on h ~position:NorthEast
          [|"mark slices"|])

let plot_cachelines h data =
  Plot.set_xlabel h "Unique skipped cachelines per slice";
  Plot.set_ylabel h "Cumulative distribution" ;
  Plot.set_title h "Cumulative distribution of unique skipped cacheline count" ;
  Plot.(ecdf ~h ~spec:[ RGB (0,0,255) ] data.cachelines)

let total_skipped_per_ratio ratios skipped =
  let arr = Array.make 100 0. in
  Mat.iter2 (fun ratio skip ->
    let i = min (int_of_float ratio) 99 in
    assert (i >= 0);
    arr.(i) <- arr.(i) +. skip ) ratios skipped ;
  Mat.of_array arr 1 100

let plot_total_skipped_per_ratio h data =
  Plot.set_xlabel h "Skip ratio (%)";
  Plot.set_ylabel h "Total skipped words" ;
  Plot.set_title h "Total skipped words per skip ratio" ;
  Plot.(bar ~h ~spec:[ RGB (255,128,0) ; FillPattern 3 ]
          (total_skipped_per_ratio data.ratios data.skipped)) ;
  let skipped_above_threshold =
    Mat.mapi (fun i x ->
      if Mat.get data.cachelines 0 i >= cacheline_threshold
      then x
      else 0.) data.skipped
  in
  Plot.(bar ~h ~spec:[ RGB (153,0,0) ; FillPattern 3 ]
          (total_skipped_per_ratio data.ratios skipped_above_threshold)) ;
  Plot.(legend_on h ~position:NorthEast
          [| "All" ;
             Printf.sprintf "Many unique cachelines (≥%d)" (int_of_float cacheline_threshold) |]
       )

let plot name data_list =
  let data = data_of_list data_list in
  let m = 4 in
  let n = 2 in
  let h = Plot.create ~m ~n name in
  Plot.set_page_size h (n * 800) (m * 600) ;
  Plot.set_font_size h 5. ;
  let subplot i j f =
    Plot.subplot h i j ;
    f h data
  in
  subplot 0 0 plot_count_vs_skipped ;
  subplot 3 0 plot_skip_ratios_vs_cachelines ;
  subplot 1 0 plot_skip_ratios ;
  subplot 1 1 plot_skip_ratios_quartile3 ;
  subplot 0 1 plot_seen_vs_skip ;
  subplot 3 1 plot_unique_vs_skipped ;
  subplot 2 1 plot_cachelines ;
  subplot 2 0 plot_total_skipped_per_ratio ;
  Plot.output h

let input_file scan name =
  (* work around stack overflow when compiling ml data file ! *)
  let x = ref [] in
  let file = open_in name in
  try
    while true do
      let line = input_line file in
      try
        scan line (fun c s l -> x := (c,s,l) :: !x)
      with _ -> ()
    done ;
    assert false
  with
  | End_of_file -> (
      if (0 = List.length !x) then
        failwith (Printf.sprintf "Import of %s failed" name) ;
      !x
    )

let scan_old line = Scanf.sscanf line "(* OCaml *) let () = x := (%d, %d, %d) :: !x"
let scan_new line = Scanf.sscanf line "seen=%d, skipped=%d, cachelines=%d"

let () =
  let plots =
    [ "world_opt", scan_old ;
      "world_opt_412", scan_old ;
      "opam_reinstall", scan_new ;
      "coq_native", scan_new ;
      "coqide", scan_new ;
      "mathcomp", scan_new ;
      "coq_mathcomp_no_native", scan_new ;
      "mathcomp_nnp", scan_new ;
      "coq_nnp", scan_new ;
    ]
  in
  List.iter (fun (name, scan) -> plot (Printf.sprintf "%s.png" name)
                                   (input_file scan (Printf.sprintf "data_%s" name)))
    plots

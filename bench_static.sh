#!/bin/sh
N=5
SKIP_RATIOS="0 1 2 5 10 20 40"
for I in $SKIP_RATIOS
do
		echo "Skip ratio:" $I"%"
		for J in $(seq 1 $N); do
#        MALLOC_CONF="thp:always" LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2 \
        ./bench strings $I 2> /dev/null | grep " ms/gc"
    done
done

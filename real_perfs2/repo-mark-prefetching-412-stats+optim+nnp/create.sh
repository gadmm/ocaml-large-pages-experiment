REPO=mark-prefetching-412-stats+nnp+optim
opam switch create . --empty &&
opam pin -y ocaml-variants.4.12.0 https://github.com/gadmm/ocaml.git#$REPO &&
opam install -y dune num zarith lablgtk3-sourceview3 &&
opam repo add coq-released https://coq.inria.fr/opam/released

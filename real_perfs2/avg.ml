type file = { name : string ; fd : in_channel }

let open_file name = { name ; fd = open_in name }

let open_file_noexc name =
  try Ok (open_file name) with e ->
  try Ok (open_file (name ^ "-0")) with _ ->
    Error (Printexc.to_string e)

(* sequence of files name, name.1, ..., name.n *)
let open_file_sequence name =
  let rec seq i () =
    try Seq.Cons (open_file (Printf.sprintf "%s-%d" name i), seq (i+1))
    with _ -> Seq.Nil
  in
  Result.map (fun f -> f :: List.of_seq (seq 1)) (open_file_noexc name)

let input_file scan file =
  let x = ref [] in
  try
    while true do
      let line = input_line file.fd in
      try
        scan line (fun c s e -> x := (c,s,Filename.basename e) :: !x)
      with _ -> ()
    done ;
    assert false
  with
  | End_of_file -> (
      if (0 = List.length !x) then
        failwith (Printf.sprintf "Import of %s failed" file.name) ;
      !x
    )

let scan line f = Scanf.sscanf line "work_done=%d, duration(ns)=%d, exe=%s" (fun w d e -> f w d e)

let tot = List.fold_left (fun (w,d,lat) (w',d',_) -> (w+w', d+d', max lat d')) (0,0,0)

let input = input_file scan

let rec sum = function
    [] -> 0.
  | n :: l -> n +. sum l

(* mean and std dev *)
let mean_std_dev_simpl l =
  let avg l = sum l /. (float_of_int (List.length l)) in
  let square n = n *. n in
  let mean_of_square = avg (List.map square l) in
  let mean = avg l in
  mean, Float.sqrt (mean_of_square -. (square mean))

(* weighted std dev *)
let std_dev mean total_weight l =
  let variation (w,d,_) =
    let wf = float_of_int w in
    let df = float_of_int d in
    if df = 0. then 0. else (
      let e = ((wf /. df) -. mean) in
      df *. e *. e
    )
  in
  let l = List.map variation l in
  let l = List.sort Stdlib.compare l in
  let sum = List.fold_left (+.) 0. l in
  let var_norm = sum /. total_weight in
  Float.sqrt var_norm

let exes l =
  let l = List.map (fun (_,_,e) -> e) l in
  List.sort_uniq Stdlib.compare l

type t = { filename : string
         ; data : (int * int * string) list
         ; words : int
         ; duration_tot : int
         ; duration_max : int
         ; pace : float
         ; mean : float
         ; std_dev : float
         ; latencies : (float * int) list
         ; exes : string list
         }

let print_throughput s d =
  Printf.printf "%s: work=%#d, duration=%#dµs, max=%#dµs, pace=%f ns/w, avg=%.2f w/µs +/- %.2f\n"
    s
    d.words
    (d.duration_tot / 1000)
    (d.duration_max / 1000)
    d.pace
    (d.mean *. 1000.)
    (d.std_dev *. 1000.)

let print_latency s d =
  Printf.printf "%s: " s ;
  List.iter (fun (percentile, duration) -> Printf.printf "%.3f%%: %#dµs. "
                                             (percentile *. 100.)
                                             (duration / 1000)) d.latencies ;
  print_newline ()

let compute_latencies data =
  let rec seq data () = match data with
    | (_,d,e) :: tl when true (*e = *)  -> Seq.Cons (d,seq tl)
    | (_,d,_) :: tl -> seq tl ()
    | [] -> Seq.Nil
  in
  let durations = Array.of_seq (seq data) in
  Array.sort Stdlib.compare durations ;
  let percentile n = 1. -. (Float.pow 0.1 (float_of_int n)) in
  let max = Array.length durations - 1 in
  let latency p = (float_of_int max) *. p |> Float.round |> int_of_float in
  let rec seq i () =
    let p = percentile i in
    let l = latency p in
    Seq.Cons ((p,durations.(l)), if l = max then (fun () -> Seq.Nil) else seq (i+1))
  in
  List.of_seq (seq 1)

let process files =
  let data = List.map input files |> List.flatten in
  let (words, duration_tot, duration_max) = tot data in
  let pace = (float_of_int duration_tot) /. (float_of_int words) in
  let mean = 1. /. pace in
  let std_dev = std_dev mean (float_of_int duration_tot) data in
  let latencies = compute_latencies data in
  let exes = exes data in
  { filename = (List.hd files).name ; data ; words ; duration_tot ;
    duration_max ; pace ; mean ; std_dev ; latencies ; exes }

let stats_two files_a files_b =
  let ad = process files_a in
  let bd = process files_b in
  print_throughput "a" ad ;
  print_throughput "b" bd ;
  print_latency "a" ad ;
  print_latency "b" bd ;
  print_endline "all data in the sequences added up" ;
  let diff_pace = (bd.pace /. ad.pace -. 1.) *. 100. in
  Printf.printf "diff pace: %.2f%%\n" diff_pace ;
  Printf.printf "%s is faster\n" (if diff_pace >= 0. then ad.filename else bd.filename) ;
  let diff_max = ((float_of_int bd.duration_max) /. (float_of_int ad.duration_max) -. 1.) *. 100. in
  Printf.printf "diff max duration: %.2f%%\n" diff_max

let print_exes l =
  Printf.printf "exes:" ;
  List.iter (Printf.printf " %s") l ;
  Printf.printf "\n"

let print_mean_error_speeds data =
  let (mean_duration, error_duration) =
    (* because the data is about identical workloads *)
    let speeds = List.map (fun d -> d.mean) data in
    mean_std_dev_simpl speeds
  in
  let (mean_std_dev, _error_std_dev (* meaning? *)) =
    (* only because the values are very close in practice *)
    let std_devs = List.map (fun d -> d.std_dev) data in
    mean_std_dev_simpl std_devs
  in
  Printf.printf "avg speed: %.2f (+/- %.2f) w/µs +/- %.2f\n" (* (+/- %.2f) *)
    (mean_duration *. 1000.)
    (error_duration *. 1000.)
    (mean_std_dev *. 1000.)

let stats_one printers fds =
  let data = List.map (fun f -> process [f]) fds in
  let all_printers s d = List.iter (fun p -> p s d) printers in
  if List.length data = 1 then
    all_printers "total" (List.hd data)
  else
    List.iteri (fun i d -> all_printers (Printf.sprintf "%d" i) d) data ;
  print_mean_error_speeds data ;
  print_exes (List.hd data).exes

let rec input_num s =
  print_endline "" ;
  print_endline s ;
  Printf.printf "? " ;
  flush stdout ;
  let input = input_line stdin in
  match int_of_string_opt input with
  | Some i when i >= 1 -> i
  | _ -> (
      Printf.printf "Please enter a number above 1\n";
      input_num s
    )

let choice_epoch () =
  let i = input_num "Choose epoch (num >= 1)" in
  Printf.sprintf ".%d" i

let rec choice l =
  print_endline "" ;
  let max = List.length l in
  List.iteri (fun i s -> Printf.printf "%d: %s\n" (i+1) s) l ;
  Printf.printf "? " ;
  flush stdout ;
  let input = input_line stdin in
  match int_of_string_opt input with
  | Some i when i >= 1 && i <= max -> (i, List.nth l (i - 1))
  | _ -> (
      Printf.printf "Please enter a number between 1 and %d\n" max ;
      choice l
    )

let input_list name =
  let f = open_in name in
  let rec get_line () =
    (* The programmer will want to catch the error code, but needs to
       open the manual to see which exception catch. The type system
       does not help! It is too tempting to reason that the only way it
       can fail is by reaching the end of file, so a _ looks quicker and
       ok! Grave mistake. *)
    try Seq.Cons (input_line f, get_line)
    with _ -> Seq.Nil
  in
  List.of_seq get_line

let show_file name =
  let f = open_in name in
  let rec show_line () =
    try print_endline (input_line f) ; show_line () with _ -> ()
  in
  show_line ()

let repos = input_list "repo_list"
let experiments = input_list "experiments_list"

let choose_repo () =
  let (_, repo) = choice repos in
  repo

let try_ f = try f () with
  | exn -> Printf.printf "Exception: %s\n" (Printexc.to_string exn)

let path = Printf.sprintf "repo-%s/%s"

let stats_file printers fds =
  try_ (fun () -> stats_one printers fds)

let rec choose_experiment epoch =
  let (_, file) = choice experiments in
  choices epoch (file ^ epoch)

and choose_epoch () =
  let () = show_file "experiments" in
  print_endline "" ;
  let epoch = choice_epoch () in
  choose_experiment epoch

and choices epoch file =
  match choice ["Stats for one repo" ;
                "Throughput for all repos" ;
                "Throughput for all repos (summary)" ;
                "Latencies for all repos" ;
                "Comparison between two repos" ;
                "Choose experiment"  ;
                "Choose epoch" ] with
  | (1, _) -> (
      let repo = choose_repo () in
      Printf.printf "Chosen: %s\n" repo ;
      print_endline "" ;
      (match open_file_sequence (path repo file) with
       | Ok fds -> stats_file [print_throughput; print_latency] fds
       | Error _ -> Printf.printf "no data\n") ;
      choices epoch file
    )
  | (2 as p,_) | (3 as p,_) | (4 as p,_) -> (
      let print =
        if p == 2 then [print_throughput]
        else if p == 4 then [print_latency]
        else []
      in
      Printf.printf "Stats for all repos\n" ;
      List.iter (fun repo ->
        print_endline "" ;
        (match open_file_sequence (path repo file) with
         | Ok fds ->
             Printf.printf "Repo: %s\n" repo ;
             stats_file print fds
         | Error _ -> Printf.printf "Repo: %s: no data\n" repo)
      ) repos ;
      choices epoch file
    )
  | (5, _) -> (
      let a = choose_repo () in
      let b = choose_repo () in
      Printf.printf "Chosen a: %s\n" a ;
      Printf.printf "Chosen b: %s\n" b ;
      print_endline "" ;
      (match open_file_sequence (path a file), open_file_sequence (path b file) with
       | Ok af, Ok bf -> try_ (fun () -> stats_two af bf)
       | ea, eb ->
           let perr = function Error s -> print_endline s | _ -> () in
           perr ea ; perr eb
      ) ;
      choices epoch file
    )
  | (6, _) -> choose_experiment epoch
  | (7, _) -> choose_epoch ()
  | _ -> assert false


let () = match Sys.argv with
  | [| _ |] -> choose_epoch ()
  | [| _; a |] -> stats_one [print_throughput; print_latency] [open_file a]
  | [| _; a; b |] -> stats_two [open_file a] [open_file b]
  | _ -> failwith "Takes one or two filenames as arguments; zero for interactive mode."

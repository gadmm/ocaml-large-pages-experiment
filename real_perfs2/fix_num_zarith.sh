unset OPAMSWITCH

for i in `cat repo_list`; do
    eval $(opam env --switch=./repo-${i} --set-switch)
    opam remove -y num zarith
    rm -rf repo-${i}/_opam/lib/num
    rm -rf repo-${i}/_opam/lib/num-top
    rm -rf repo-${i}/_opam/lib/zarith
    rm -rf repo-${i}/_opam/lib/stublibs/dllzarith.so*
    opam reinstall -y zarith
done

unset OPAMSWITCH

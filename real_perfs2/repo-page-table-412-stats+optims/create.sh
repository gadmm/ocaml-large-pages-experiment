REPO=page-table-412-stats+optims
opam switch create . --empty &&
opam pin -y ocaml-variants.4.12.0 https://github.com/gadmm/ocaml.git#$REPO &&
opam install -y dune num zarith lablgtk3-sourceview3 &&
opam repo add coq-released https://coq.inria.fr/opam/released

#!/bin/bash

# Config

set -x

EPOCH=.16 # appended to the names of all log files

LOGFILE=install.log${EPOCH}
# log session using script
if [ -z "$SCRIPT" ]; then
    # exit if log file already exists
    if [ -f "$LOGFILE" ]; then
        echo "$LOGFILE already exists."
        exit 1
    fi
    SCRIPT=1 /usr/bin/script ${LOGFILE} /bin/bash -c "$0 $*"
    exit 0
fi

#REPOS=`cat repo_list`
REPOS="mark-prefetching-412-stats page-table-412-stats+optims+force-huge-pages" #"mark-prefetching-412-stats+nnp page-table-412-stats+optims+nnp page-table-412-stats+forward_optim page-table-412-stats+optims mark-prefetching-412-stats+optim+nnp" # mark-prefetching-412-stats page-table-412-stats+fancy2
TRIALS=3
# compilation
USE_CLANG=false
USE_MARCH=false
USE_ALIGN=false
REINSTALL=false # must be true if compilation options are changed
# benchmarks
BENCH_REINSTALL=false
BENCH_GALLINA_OPAM=true
BENCH_MATHCOMP=true
BENCH_MATHCOMP_HEAVY=false

DRY= #--dry-run

# Configure your CPU to avoid scaling and throttling
#   cpufreqctl --governor --set=performance &&
#   cpufreqctl --max-perf --set=100 &&
#   cpufreqctl --min-perf --set=100 &&
#   cpufreqctl --no-turbo --set=1
# set J low enough
# make sure the IS_THROTTLED control works
J=4

# Internals

THROTTLE_CMD="cpufreqctl --throttle" # number of times the CPUs have
                                     # been throttled
TIME=time

GALLINA_C="coq coqide"

GALLINA_PKGS="coq-mathcomp-field coq-mathcomp-solvable
              coq-mathcomp-algebra coq-mathcomp-fingroup
              coq-mathcomp-ssreflect"

GALLINA_HEAVY_PKGS="coq-mathcomp-character coq-mathcomp-odd-order"

export OPAMVAR_ocamlas="as"

MARCH=""

if $USE_MARCH; then
    MARCH="-march=tigerlake -fgcse-after-reload -fipa-cp-clone -floop-interchange \
           -floop-unroll-and-jam -fpeel-loops -fpredictive-commoning \
           -fsplit-loops -fsplit-paths -ftree-loop-distribution \
           -ftree-loop-vectorize -ftree-partial-pre -ftree-slp-vectorize\
           -funswitch-loops -fvect-cost-model -fvect-cost-model=dynamic \
           -fversion-loops-for-strides" # gives a different code
                                        # layout, essentially
                                        # "-march=native -O3" plus
                                        # stricter function and loop
                                        # alignment (note that we
                                        # cannot specify -O3 via the
                                        # CC argument)
fi

if $USE_MARCH && $USE_CLANG; then
    MARCH="-march=tigerlake -fslp-vectorize"
fi

ALIGN=""

if $USE_CLANG; then
    if $USE_ALIGN; then ALIGN="-falign-functions=16"; fi
    export OPAMVAR_ocamlcc="clang ${MARCH} ${ALIGN}"
else
    if $USE_ALIGN; then ALIGN="-falign-functions=16 -falign-loops=16 -falign-jumps=16"; fi
    export OPAMVAR_ocamlcc="gcc ${MARCH} ${ALIGN}"
fi

unset OPAMSWITCH

throttle_control_begin(){
    IS_THROTTLED=
    THROTTLE=`$THROTTLE_CMD`
}

throttle_control_end(){
    if [ $THROTTLE -eq `$THROTTLE_CMD` ]
    then IS_THROTTLED=
    else IS_THROTTLED=-throttled
    fi
}

rm /tmp/ocaml-stats-*.log

# (Re)install ocaml-variants, assumed invalid as an experiment
for i in $REPOS; do
    cd repo-${i}
    eval $(opam env --switch=. --set-switch)
    opam list
    opam remove $DRY -y coq num zarith
    # fix opam/packaging bug (???)
    rm -rf $DRY _opam/lib/zarith
    rm -rf $DRY _opam/lib/stublibs/dllzarith.so*
    if $REINSTALL; then c=reinstall; else c=install; fi
    opam $c $DRY -y ocaml-variants zarith lablgtk3-sourceview3 dune || exit 1
    rm $DRY /tmp/ocaml-stats-${i}.log
    cd ..
done

for n in $(seq 0 $((TRIALS - 1))); do
    TRIAL=-${n}

    echo "Iteration ${n}"
    date -R

    if $BENCH_REINSTALL; then
        # Reinstall ocaml-variants, benchmarked
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            opam remove $DRY -y coq num zarith
            # fix opam/packaging bug (???)
            rm -rf $DRY _opam/lib/zarith
            rm -rf $DRY _opam/lib/stublibs/dllzarith.so*
            $TIME opam reinstall --skip-updates -j $J $DRY -y ocaml-variants zarith
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log reinstall-ocaml-variants.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

    if $BENCH_GALLINA_OPAM || $BENCH_MATHCOMP || $BENCH_MATHCOMP_HEAVY; then
        # Compile Gallina >= 8.15 without coq-native (nnp compatible)
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            if $BENCH_GALLINA_OPAM; then
                opam remove $DRY -y $GALLINA_PKGS $GALLINA_HEAVY_PKGS
                c=reinstall
            else
                c=install
            fi
            $TIME opam $c --skip-updates $DRY -j $J -y $GALLINA_C
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log coq-opam.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

    if $BENCH_MATHCOMP; then
        # Compile a few Mathcomp packages
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            opam remove $DRY -y $GALLINA_HEAVY_PKGS
            $TIME opam reinstall --skip-updates $DRY -j $J -y $GALLINA_PKGS
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log coq-mathcomp.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

    if $BENCH_MATHCOMP_HEAVY; then
        for i in $REPOS; do
            throttle_control_begin
            cd repo-${i}
            eval $(opam env --switch=. --set-switch)
            opam list
            $TIME opam reinstall $DRY -j $J -y $GALLINA_HEAVY_PKGS
            throttle_control_end
            mv $DRY /tmp/ocaml-stats-${i}.log coq-mathcomp-heavy.log${EPOCH}${TRIAL}${IS_THROTTLED}
            cd ..
        done
        date -R
    fi

done # TRIALS

unset OPAMSWITCH

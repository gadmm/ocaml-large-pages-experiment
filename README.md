# OCaml large pages experiment

Experiments with large pages for heap allocation in OCaml, measuring
performance improvements.

**Note: this work has been presented at the OCaml workshop 2022 under
the title *Efficient “out of heap” pointers for multicore OCaml*, and
a shorter and more recent article is [available
here](https://guillaume.munch.name/files/large-pages-ocamlworkshop.pdf)
(PDF). An experimental branch of OCaml 4.14 with a fast page table,
and huge pages enabled by default for convenience, is available here:
https://github.com/gadmm/ocaml/tree/page-table-414-huge-pages (it uses
huge pages if the value of
`/sys/kernel/mm/transparent_hugepage/enabled` is at least
`madvise`).**

Large pages affect both the performance of OCaml's page table (which
currently has a fairly expensive implementation in 64-bits), and the
TLB / process page table. We also manage to extrapolate the
hypothetical cost of a page table in multicore OCaml.

We conclude that:

* Transparent huge pages can bring a significant performance
  improvement, whether by using a different malloc or by a direct
  implementation in the OCaml runtime.
* Contrary to expectations, an optimised page table yields a
  (negligible) speedup compared to the no-naked-pointers mode with the
  original implementation. Further performance gains in our experiment
  come from the skipping the marking of static data. Analysis of
  real-world workloads show that static data is encountered fairly
  frequently during marking, however it is unclear that the speedup is
  as important in real-world programs.
* The good performance of the page table scales to large heaps and
  multiple parallel domains (multicore).

## Table of contents

[[_TOC_]]

## Setup

The baseline is Stephen Dolan's PR
[#10195](https://github.com/ocaml/ocaml/pull/10195) *“Speeding up GC
by prefetching during marking”* (in the version of April 2021) and the
benchmark is the one proposed there. This PR rewrites the core marking
loop of the OCaml GC to optimise memory access (mainly) and
computation costs. The page table is then expected to have an
extravagant cost.

The role of OCaml's page table in the marking loop is to test whether
a value belongs to the major heap before marking it. The
“no-naked-pointers” mode avoids the page table by letting the GC mark
outside of the heap, with some restrictions on allowable out-of-heap
pointers that ensure that marking is a noop. More context is given in
my ML 2020 paper.

The benchmark allocates roughly 800MB of values distributed randomly,
and so constitutes a worst-case in terms of cache locality. Its output
is the average time for a full GC over 5 runs.

The `Makefile` lets you configure OCaml (32-bit vs 64-bit, page table
vs. no-naked-pointers), and then it lets you compile OCaml and run the
benchmark for the choice of options. The `Makefile` expects that the
checkout of the various experimental OCaml branches (referred to
below) is in the parent directory.

The best result out of 5 (more or less) is kept. Results are gathered
in the Calc file `results.odc` (which we round here to significant
digits). This was chosen to best eliminate fluctuations on the
available test machine (a laptop).

## Test machine

The OS is Linux 5.8.0-50-generic. The processor is Intel Core i7-6600U
CPU @ 2.60GHz (a Skylake processor whose MLP characteristics are
similar to the one used to report figures in the above PR). The
`Makefile` applies the workaround to the Intel JCC bug mentioned in
the PR.

Transparent huge pages settings are:
* `/sys/kernel/mm/transparent_hugepage/enabled` -> `madvise`
* `/sys/kernel/mm/transparent_hugepage/defrag` -> `madvise`

## Experiment 1 : Cost of page table before and after the prefetching patch

* *Mark-prefetching*: the PR #10195 as of April 2021.
  (https://github.com/gadmm/ocaml/tree/mark-prefetching)
* *Before*: the base of the above branch.
  (https://github.com/gadmm/ocaml/tree/mark-prefetching-before)

### 64-bit

| 800MB            | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---             | ---:           | ---:                  | ---:              |
| Before           | 3.91           | 3.74                  | **0.16**          |
| Mark-prefetching | 1.04           | 0.65                  | **0.38**          |

On 64-bit, the page table is implemented with a hash table with linear
probing. The page table is sized between 2 to 4 times the number of
elements (each representing a 4KB page), which makes the collisions
fairly frequent, and no optimisation such as inlining the fast path is
performed.

The page table incurs a cost of more than 2.3 times the duration
before the PR. As described in the original PR, the prefetching
algorithm is not optimised for the page table, and indeed, checking
values against the page table is done during the fast loop to request
prefetching, such that we can emit the hypothesis that its own memory
accesses and computational costs reduce the benefits of prefetching
(which is confirmed in the subsequent experiment).

### 32-bit

| 400MB            | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---             | ---:           | ---:                  | ---:              |
| Before           | 2.75           | 2.79                  | **-0.04**         |
| Mark-prefetching | 0.62           | 0.69                  | **-0.07**         |

On 32-bit the page table is implemented with a 2-lvl bibop of 4KB
pages. The page table provides a noticeable speed-up, which is
contrary to expectations. This can be explained by the low cost of the
page table query (the fact that it is inlined, and more importantly
its good cache locality, with one cache line storing 64 neighbouring
pages), and the fact that the page table skips the marking of static
data (which is investigated later on).

The original benchmark allocates a fixed amount of words, and so in
32-bit this means that roughly 400MB are allocated only in the above
test. This increases the benefits of caching compared to 800MB. After
doubling the size of the heap, we still find that the page table
provides a speed-up of about 10%:

| 800MB            | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---             | ---:           | ---:                  | ---:              |
| Mark-prefetching | 1.37           | 1.49                  | **-0.12**         |

All the remaining experiments are for 64-bit.

## Experiment 2 : simple optimisations to the page table for prefetching

Two simple optimisations of the page table to make it better-behaved
with respect to prefetching are experimented.

* *Prefetch*: prefetch the page table entry at the same time the value
  is prefetched, and perform the test later. This is a simple
  modification to the prefetching algorithm, which also requires for
  performance to move the page table definition to the headers for
  better inlining. There are no downsides compared to
  Mark-prefetching.
  (https://github.com/gadmm/ocaml/tree/page-table-prefetch-simple)

* *Large pages*: the idea is to mimic the cache locality of the 32-bit
  page table. Pages for the heap are counted in 256KB blocks (64 OS
  pages). This leads to a page table which contains 64 times fewer
  elements. Instead of dividing the size of the page table by 64, we
  divide it by 8 only, which significantly reduces the chance of
  collisions (and significantly improves its performance). The
  downside is that it incurs more wasted memory to obtain aligned
  chunks with the technique currently in use in the OCaml runtime (64
  times more: 256KB per chunk).
  (https://github.com/gadmm/ocaml/tree/page-table-large-pages-no-prefetch)

* *Large pages + prefetch*: both combined
  (https://github.com/gadmm/ocaml/tree/page-table-large-pages-prefetch)

| 800MB                  | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---                   | ---:           | ---:                  | ---:              |
| (Before)               | 3.91           | 3.74                  | 0.16              |
| (Mark-prefetching)     | 1.04           | 0.65                  | 0.38              |
| Prefetch               | 0.89           | 0.65                  | **0.23**          |
| Large pages            | 0.74           | 0.65                  | **0.09**          |
| Large pages + prefetch | 0.88           | 0.65                  | **0.23**          |

The simplest strategy (Prefetch) is still more expensive than before
in apparence (0.23s vs. 0.16s). On the other hand, its gains (0.15s)
are comparable to those of removing the page table before the patch.

The Large pages strategy shows that it is more beneficial to increase
the cache locality and reduce the computation cost of the page table,
than do a good prefetching. Its benefits are much greater than the
prefetching strategy, almost halving the cost of the page table
compared to before (0.09s vs 0.16s). While this optimisation is not
much more complex than Prefetch, it has a downside of wasted memory. A
smaller factor than 64 OS pages per entry might still provide gains
whilst being more realistic in terms of wasted memory.

The combination of Large pages and Prefetch negates the gains of large
pages. This suggests that we are limited by the computational costs
rather than the memory access costs: prefetching requires to compute
the Hash in two separate locations. (It would also be possible to
store the address of the page table entry in the prefetch buffer, but
this change is fairly invasive to the marking loop and its gains,
although existent, were limited, so we did not pursue it further.)

### Performance in real-world programs with the Coq benchmark

Lastly, we rebased two implementations on top of OCaml 4.12.0 for use
in real-world programs:
- *Mark-prefetching-412*, the original prefetching strategy
  (https://github.com/gadmm/ocaml/tree/mark-prefetching-412).
- *Prefetch-412*, the minimal modification to the prefetching strategy that
  is more friendly to the page table
  (https://github.com/gadmm/ocaml/tree/mark-prefetching-bis-412).

The Coq benchmark was run on the test machine used by the Coq
developers (courtesy of Pierre-Marie Pédrot for setting up and running
the tests):

|                      | User time         | CPU cycles    | CPU instructions |
| :---                 | ---:              | ---:          | ---:             |
| Mark-prefetching-412 | **-0.8%** (σ=1.1) | -0.8% (σ=1.0) | -0.9% (σ=1.1)    |
| Prefetch-412         | **-1.8%** (σ=1.6) | -1.7% (σ=1.5) | -1.0% (σ=1.3)    |

- The test machine is a dedicated server with an Intel Xeon E5-1603
  and 64GB of RAM.

- The benchmarking suite comprises 35 tests, only 27 of which had
  exploitable data due to manipulation error (unfortunately, running
  the tests again was expensive, so we chose to use whichever reliable
  data was available to us). The results are given as a difference
  with OCaml 4.12.0 (`ocaml-base-compiler.4.12.0`). We reported the
  mean and standard deviation over the 27 tests.

- Coq currently needs OCaml with naked pointers to run, so all the
  figures are with the page table.

- The benchmarks have large heaps, ranging from 370MB to 2800MB,
  making it suitable for testing the impact of prefetching.

- By default, Coq uses aggressive GC parameters: a minor heap size of
  256MB and a space overhead of 200. This reduces the cost of major
  collections, and so diminishes the gains we can expect, despite the
  large heaps.

In this benchmark, differences of more than 1% in user time are
considered significant by Coq developers. This concerns 8 tests out of
27 for Mark-prefetching, and 20 tests out of 27 for Prefetch.

With the original prefetching strategy, we observe that gains in user
time are limited to the gains in CPU instructions. With the simple
modification to prefetching to make it friendlier to the page table,
we observe gains from prefetching (as seen from the increase in
instructions per cycle). We emit the hypothesis that in real-word
scenarios in terms of cache locality, querying the current 64-bit page
table at an inopportune moment, as in the original strategy, defeats
prefetching.

## Experiment 3: huge pages with `mmap`

We now allocate the heaps (both minor and major) with a bigger
granularity (a multiple of the huge OS page size, typically 2MB). We
can thus measure the benefits of using huge OS pages. Using huge pages
(2MB) instead of small pages (4KB) benefits performance by reducing
the amount of TLB misses.

* *Madvise*: we change the heap allocation functions to use `mmap` to
  commit requested memory in multiples of 2MB, aligned to huge pages.
  This ensures that no memory is wasted. We also request transparent
  huge pages (THP) using `madvise`. The heaps are the typical large
  allocations for which THP is recommended. This concerns both the
  minor heap and the major heap.
  (https://github.com/gadmm/ocaml/tree/virtual-huge-pages)

* *Jemalloc*: the mark-prefetching branch but using jemalloc with the
  option `thp:always`. (See the commented line in the `Makefile`.)

| 800MB              | No-naked-pointers (s) | Speedup | dTLB miss |
| :---               | ---:                  | ---:    | ---:      |
| (Mark-prefetching) | 0.65                  |         | 0.64%     |
| Madvise            | 0.55                  | **16%** | 0.00%     |
| Jemalloc           | 0.58                  | **11%** | 0.00%     |

With a larger heap:

| 6.4GB              | No-naked-pointers (s) | Speedup | dTLB miss |
| :---               | ---:                  | ---:    | ---:      |
| (Mark-prefetching) | 9.02                  |         | 2.20%     |
| Madvise            | 6.37                  | **29%** | 0.32%     |
| Jemalloc           | 7.11                  | **21%** | 0.33%     |

The last column shows the dTLB load miss ratio as recorded with the
Linux `perf` tool (`dTLB-load-misses`). With the 6.4GB heap, the dTLB
miss ratio is quite high even with huge pages. The measure of
`dtlb_load_misses.walk_completed_4k` was very high, showing that the
memory was too fragmented for the system to succeed in allocating the
heap entirely with huge pages.

First, transparent huge pages bring significant benefits.
Surprisingly, jemalloc with the `thp:always` option does not seem to
offer the largest benefits, even though it has previously been shown
to almost eliminate the dTLB load misses. We managed to locate the
cause of this difference in measurement biases of the original
benchmark; refined benchmarks (see Section "Final results") now show
similar figures for Madvise and Jemalloc.

Second, one should note that the GC is not the only one to benefit from
huge pages, the mutator does too.

Third, THP brings the most important benefits with the larger heap,
meaning that the OS has to make more efforts to back memory with huge
pages. This has a cost. In the test setup, we had `defrag` configured
to `madvise`, meaning that the OS can stall while trying to compact
memory to free up huge pages. On a system with large uptime, this has
been seen to perceptibly stall the extension of the major heap
(meaning that the latency could be in the hundreds of milliseconds).
Another value for `defrag`, `defer`, would not stall allocations but
cause background activity from low-priority kernel processes to
progressively replace these pages with huge pages as they become
available, and compact memory to free up huge pages.

This suggests two options to improve huge page support in OCaml.
Either by using the Madvise strategy everywhere (and warning users to
pay attention to their `defrag` settings). Or for maximum
backwards-compatibility as a fallback for the current mecanism of the
`H` option (the `H` option currently has no fallback and simply fails
if it cannot allocate reserved (hugetlb) huge pages). In both cases
THP is very suitable for the OCaml heaps, in particular because they
are allocated and deallocated (through compaction) in large contiguous
chunks.

(An earlier version of this document stated that one already gets
benefits merely from aligning allocations on huge page boundaries.
This was discovered to be due to measurement biases in the original
benchmark. Refined benchmarks from Section "Final results" show
negligible benefits from merely aligning memory chunks to huge page
boundaries.)

## Experiment 4: efficient page table with virtual address space reservation for multicore

Building upon the large pages strategy, we now use virtual address
space reservation with `mmap` to experiment with much more efficient
page table implementations.

We use `mmap` to *reserve* heap chunks at a granularity N >> 2MB,
aligned at N, out of which requested memory is *committed* in
multiples of 2MB. This ensures that no memory is wasted compared to
the aligned allocation function in trunk. (N=256MB in our experiments,
but this can be chosen smaller.)

* *Perfect hashing*: we make a simple change to the page table from
  trunk to perform perfect hashing instead of linear probing. Having
  much fewer elements, we replace the 2MB page table growing linearly
  (with costs due to collisions growing logarithmically) with a 16KB
  page table growing quadratically (with costs scaling linearly).
  Among the downsides, the quadratic growth is bad for heaps of tens
  of GB, and its synchronisation would cause difficulties in
  multicore.
  (https://github.com/gadmm/ocaml/tree/page-table-virtual-perfect-hashing)

* *Bibop*: a 2-dimensional table indexed by the most significant bits
  of the address. This is the 32-bit implementation of the page table,
  and is also the data structure used in the Go runtime. It can
  possibly scale to multicore using the technique from the Flat
  strategy below.
  (https://github.com/gadmm/ocaml/tree/page-table-bibop)

* *Bitmap*: a 128KB bitmap of the address space, where 0 means “not in
  heap” and 1 means “in heap”. Each of the 32 pages of the bitmap are
  initially mapped to the zero page, and so does not cost memory
  initially. This relies on overcommitting so is not very portable,
  but on systems without overcommitting it “only” costs 128KB of
  memory. Also it only supports two values per entry and so our
  strategy for multicore below does not apply. At least it was fun to
  optimise the generated assembly.
  (https://github.com/gadmm/ocaml/tree/page-table-bitmap)

* *Flat*: a 1-dimensional table indexed by the most significant bits
  of the address. We use on-demand paging to allocate only what is
  needed. (This is essentially like the bibop, but relying on the
  process page table and page protection tricks to implement the first
  level.) We also implement the monotonicity of the page table and
  “tainting”: any first access to an entry in the page table
  determines its value permanently, if necessary by setting the entry
  to a special value if it refers to memory outside of the heap,
  preventing the heap from getting this address in the future. As a
  consequence, the fast path only requires a relaxed load. The “slow”
  (or slightly less fast) path (involving an acquire-release
  `compare_exchange`) only occurs at most at the first read of an
  entry of the table, which demonstrably occurs infrequently
  ([source](https://github.com/gadmm/ocaml/commit/31488ced7f668c827e43372c101e0f1874f32a50#diff-dba43ce753R120-R152)).
  So, *we can deduce that the synchronisation costs are negligible and
  that figures are indicative of expected performance in multicore*.
  (Monotonicity and tainting allow efficient synchronisation, but they
  also give a good chance to fail fast in case of bugs, although they
  cannot ensure safety by themselves.)
  (https://github.com/gadmm/ocaml/tree/page-table-flat)

The virtual+bibop design was suggested in the ML 2020 paper on naked
pointers, as well as the monotonicity condition of the last design for
efficient synchronisation.

All the strategies use the Madvise strategy for huge pages from the
previous experiment, which reduces the impact of physical memory
fragmentation on the results (which varies with the uptime of the test
machine). An auxiliary data structure is used for static data which
remains mapped in 4KB pages (and could be further optimised, but the
benchmark does not let us measure that, so we left it out of scope).

These prototypes do not implement features orthogonal to performance,
such as virtual space re-use. A hint is given to mmap to reserve
chunks close from each other, so in practice with the Flat and Bibop
strategy we do not need more than 8-12KB and 12-16KB (respectively) of
memory for the page table (disregarding entries for possible foreign
pointers) even for very large heaps. Compare this to the current page
table design which consumes 4-8MB *per GB of heap*. They also have
much simpler implementations.

| 800MB              | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---               | ---:           | ---:                  | ---:              |
| (Before)           | 3.91           | 3.74                  | 0.16              |
| (Mark-prefetching) | 1.04           | 0.65                  | 0.38              |
| Perfect hashing    | 0.55           | 0.53                  | **0.02**          |
| Bibop              | 0.55           | 0.53                  | **0.01**          |
| Bitmap             | 0.53           | 0.53                  | **0.00**          |
| Flat               | 0.53           | 0.53                  | **0.00**          |

The major GC time is essentially divided by 2 compared to the
prefetching PR, and the cost of the page table is made negligible.

We then measured how the Flat design scales with the size of the heap.

| Flat           | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---           | ---:           | ---:                  | ---:              |
| (800MB)        | 0.53           | 0.53                  | 0.00              |
| 4GB            | 2.86           | 2.75                  | **0.11**          |
| 6.4GB          | 4.68           | 4.61                  | **0.07**          |
| 11GB (no THP*) | 14.361         | 14.747                | **-0.39**         |

*: All experiments with 11GB heaps are with the Align rather than
Madvise strategy (no call to madvise for THP). In the test machine
(limited to 16GB of RAM), allocating the full heap with THP with
default settings would take at least 15 minutes (we stopped the test
before it could succeed).

This shows good scalability. But intigued by the speedup of the page
table over no-naked-pointers, we ran the experiment several times, and
found consistent results. Moreover, the speedup is not obtained by
using the Align strategy with other heap sizes. We investigated the
speedup next.

## Experiment 5: impact of skipping static data

To try to explain the speedup in Experiment 1 for 32-bit and
Experiment 4 for 11GB heaps, we emit and test the hypothesis that
marking with the page table can go *faster* than the no-naked-pointers
mode, due to the page table skipping the marking of static data (which
is a noop that costs a memory access).

First we modified the OCaml compiler to emit statistics on skipped
values, measuring the proportion of values skipped by the page table
during marking (*skip ratio*), the number of distinct cache lines
containing skipped values, and a measure of uniqueness of each skipped
value. (https://github.com/gadmm/ocaml/tree/count_skipped)

The benchmark displayed a skip ratio of 60%, whilst most of the
skipped values were the same two float literals (0% uniqueness). This
means that while the page table would skip the marking of most
encountered values, performance could not benefit from skipping since
they are almost always already in cache in the no-naked-pointers mode,
making the marking of the static data free (except as observed in
Experiment 1 for 32-bit, which is more computation-bound). We need to
address this peculiarity of the original benchmark for our own
experiments.

We looked at the statistics emitted during the compilation of the
OCaml compiler itself. There, the skip ratio was most often anywhere
between 2% and 20% (sometimes more), with a much higher ratio of
unique cacheline per skipped value. The number of distinct cachelines
of referenced static data was often in the several thousands. (A raw
log is provided in the file `skip_count_and_uniqueness_ocaml`.)

We now test:

* *No static*: Replacing floats by ints in the benchmark, bringing the
  skip ratio to 0%
  (https://gitlab.com/gadmm/ocaml-large-pages-experiment/-/tree/nostatic).

* *Static strings*: Replacing floats by ints, except for one field
  which now contains a string. The string is chosen randomly among
  3000 cacheline-sized strings literals. As a result, the skip ratio
  is quite high (24%) and the reported distinct cacheline count is
  around 3000 with a high uniqueness ratio. This matches statistics
  emitted sometimes during the compilation of the OCaml compiler. (Why
  24% instead of 10% or 20%? We sought to make a simple change to the
  original benchmark to keep it comparable, and see if its statistics
  were realistic. 24% is close enough to some ratios observed in
  practice, and lowering the ratio in a non-artificial way would
  require a bigger change to the benchmark.)
  (https://gitlab.com/gadmm/ocaml-large-pages-experiment/-/tree/static_strings)

In both cases we ran the benchmark with the Flat design.

| No static     | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---          | ---:           | ---:                  | ---:              |
| 800MB         | 0.52           | 0.51                  | **0.00**          |
| 6.4GB         | 4.48           | 4.44                  | **0.04**          |
| 11GB (no THP) | 12.94          | 12.57                 | **0.38**          |

| Static strings | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---           | ---:           | ---:                  | ---:              |
| 800MB          | 0.52           | 0.54                  | **-0.02**         |
| 6.4GB          | 4.48           | 4.59                  | **-0.12**         |
| 11GB (no THP)  | 13.18          | 12.67                 | **0.51**          |

The cost of the page table remains negligible in the absence of static
data to skip. In the presence of one possible realistic use of static
data, there is an observable speedup. While small in proportion, it
outweights the cost of the table by a factor of 4, for a high skip
ratio. We can therefore conclude that the page table pays for itself.
This excludes the outlier case of 11GB heaps, which is investigated
next.

## Experiment 6: impact of contiguous allocation

We took a wild guess that a fragmented committed VAS has a cost for
page table walks, and that these page table walks are the limiting
factor in the 11GB case (due to limitations of the test machine). This
motivated committing memory for the heap in 256MB chunks instead of
letting OCaml allocate lots of smaller and disjoint chunks.
(https://github.com/gadmm/ocaml/tree/page-table-flat-bigger-chunks)

Here are the results for the flat page table + 256MB chunks:

| No static     | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---          | ---:           | ---:                  | ---:              |
| 800MB         | 0.50           | 0.50                  | **0.00**          |
| 6.4GB         | 4.23           | 4.21                  | **0.02**          |
| 11GB (no THP) | 7.68           | 7.59                  | **0.09**          |

| Static strings | Page table (s) | No-naked-pointers (s) | Absolute diff (s) |
| :---           | ---:           | ---:                  | ---:              |
| 800MB          | 0.51           | 0.55                  | **-0.04**         |
| 6.4GB          | 4.27           | 4.35                  | **-0.08**         |
| 11GB (no THP)  | 7.82           | 7.99                  | **-0.18**         |

The improvement obtained by reserving memory contiguously for a 11GB
heap without THP (in the benchmark with many static strings) is
**41%** with the page table (13.18s vs 7.82s) and **37%** in the
no-naked-pointers mode (12.67s vs 7.99s).

To validate the hypothesis that a less fragmented VAS is more
efficient for the (hardware) page table, we compared the costs
associated to the dTLB misses using the Linux `perf` tool (see
`Makefile` for the command line used), between the Flat and the Flat +
256MB chunks versions for a 11GB heap.

We measured that the cycles spent walking the table after a dTLB miss
(`dtlb_load_misses.walk_active`) was reduced by 60%, and that the
difference in number of cycles matched the difference in total number
of elapsed cycles. At the same time, the ratio of small pages to large
pages (more precisely
`dtlb_load_misses.walk_completed_2m_4m`/`dtlb_load_misses.walk_completed_4k`)
remained constant. We conclude that committing in 256MB chunks did not
improve huge page allocation, but caused page table walks to complete
more quickly. This reduced both the cost of dTLB misses, and the
number of dTLB misses occurring in parallel. (The raw data is recorded
in the file `effect_of_bigger_chunks_on_page_walks`)

This effect can be explained by improved behaviour for the MMU cache
(Barr, Cox and Rixner, *“Translation caching: skip, don't walk (the
page table)”*, ISCA 2010). This paper suggests that the ideal size of
the reserved chunk size for MMU cache behaviour would be 1GB on x86-64
(L4 idx + L3 idx), which is a size that would also simplify support
for the largest huge pages (which are 1GB on x86-64).

Whilst committing heap memory in 256MB or 1GB chunks is not realistic
as a global setting (and there is already a runtime parameter to
control the size of heap chunks), these results suggest that the best
of both worlds might be obtained by implementing the extension of the
heap by committing already-reserved contiguous virtual memory. In
particular memory reservation is beneficial to both the page table and
the no-naked-pointers mode. Similar effects (and more) can be obtained
through compaction, but as of writing, there is a plan to remove
compaction with multicore OCaml.

We have to keep in mind, though, that its predominant effect here is
first due to the shortage of huge pages to allocate such large heaps
(a realistic scenario when available memory is low, or THP is
disabled), and that the nature of the benchmark exaggerates cache
misses. The effect might be negligible in more common scenarios. In
addition, the prefetch buffer reduces the cost of TLB misses during
marking. The mutator, however, directly benefits from faster page
table walks.

## Final results

We let the results rest for a while and later went on to measure one
last time the overall gains in execution time for a full major GC. We
brought the following improvements to the implementation and to the
benchmarks:

* Contiguous allocation as in Experiment 6 is now implemented by
  serving reserved VAS chunks with a best-fit allocator. This
  allocator has a concise implementation thanks to the reuse of
  runtime data structures.
  (https://github.com/gadmm/ocaml/tree/page-table-vas-alloc)

* We identified an additional improvement to the marking loop which is
  possible with the page table. Essentially, even in no-naked-pointers
  mode, a test is required to skip young values. The page table turned
  out to be more efficient than the optimised test from the
  mark-prefetching branch.

* The benchmark was improved to better measure performance benefits
  across implementations (in particular by better controlling the
  effects of varying major heap chunk sizes and minor heap size).

* We now have some data about real-world usage of static values in
  programs with large heaps. The branch
  https://github.com/gadmm/ocaml/tree/skipped_stats records statistic
  about static data usage during marking: for each mark slice, the
  number of words seen, the number of words skipped thanks to the page
  table, and the number of unique distinct cachelines skipped in this
  slice. Here is for instance the results for a workload consisting of
  compilation of Coq followed by compilation of Coq libraries using
  Coq ([details](skip_count/data_coq_mathcomp_no_native.stdout)),
  which use large major heaps ranging from 370MB to 950MB:
  ![Compilation of Coq and Mathcomp without coq-native](skip_count/coq_mathcomp_no_native.png "Compilation of Coq and Mathcomp without coq-native")
  Other real-world intensive benchmarks show a total skip ratio
  between 3.6% and 6% with similar peaks at higher skip ratios (up to
  40%). (For more graphs see <https://gitlab.com/gadmm/ocaml-large-pages-experiment/-/tree/master/skip_count>.)

* The Static strings benchmark now lets us choose the skip ratio among
  0%, 1%, 2%, 5%, 10%, 20% and 40% (actually 19.2% and 45.4% for the
  latter two for technical reasons). We kept a number of distinct
  cachelines of static data of 3000, which together with a skip ratio
  of 5% matches the real-world workloads we tested. (Note that it is
  difficult to reproduce real-world conditions regarding cache
  locality and branch prediction using a synthetic benchmark.)

Without huge pages, 800MB heap (ms):

| Skip ratio                               | 0%      | 1%   | 2%   | 5%      | 10%  | 20%  | 40%  |
| :---                                     | ---:    | ---: | ---: | ---:    | ---: | ---: | ---: |
| mark-prefetching (page table)            | 441     | 444  | 443  | 448     | 453  | 466  | 527  |
| mark-prefetching (no-naked-pointers)     | 327     | 331  | 331  | 335     | 343  | 353  | 426  |
| page-table-vas-alloc (no-naked-pointers) | 327     | 330  | 331  | 333     | 344  | 354  | 428  |
| page-table-vas-alloc (page table)        | 323     | 323  | 325  | 325     | 326  | 330  | 349  |
| page table vs. no-naked-pointers         | **-1%** | -2%  | -2%  | **-3%** | -5%  | -7%  | -18% |

With transparent huge pages (with jemalloc for mark-prefetching, and
option H for page-table-vas-alloc), 800MB heap (ms):

| Skip ratio                               | 0%      | 1%   | 2%   | 5%      | 10%  | 20%  | 40%  |
| :---                                     | ---:    | ---: | ---: | ---:    | ---: | ---: | ---: |
| mark-prefetching (page table)            | 398     | 398  | 399  | 402     | 407  | 405  | 469  |
| mark-prefetching (no-naked-pointers)     | 295     | 297  | 300  | 301     | 306  | 316  | 377  |
| page-table-vas-alloc (no-naked-pointers) | 295     | 297  | 298  | 300     | 304  | 315  | 376  |
| page-table-vas-alloc (page table)        | 292     | 292  | 292  | 293     | 293  | 295  | 309  |
| page table vs. no-naked-pointers         | **-1%** | -2%  | -2%  | **-3%** | -4%  | -6%  | -18% |

Here is the relative improvement of page-table-vas-alloc with page
table and transparent huge pages:

| Skip ratio                               | 0%   | 5%       | 40%  |
| :---                                     | ---: | ---:     | ---: |
| vs. mark-prefetching (page table)        | -34% | **-35%** | -41% |
| vs. mark-prefetching (no-naked-pointers) | -11% | **-13%** | -28% |

One should keep in mind that the results are for one particular
configuration: we noticed that by simply changing the CPU scaling
governor from `performance` to `powersave`, we obtained an increase in
relative performance:

| Skip ratio                               | 0%      | 1%   | 2%   | 5%      | 10%  | 20%  | 40%  |
| :---                                     | ---:    | ---: | ---: | ---:    | ---: | ---: | ---: |
| page-table-vas-alloc (no-naked-pointers) | 375     | 378  | 381  | 386     | 399  | 417  | 523  |
| page-table-vas-alloc (page table)        | 367     | 368  | 368  | 369     | 373  | 378  | 418  |
| page table vs. no-naked-pointers         | **-2%** | -3%  | -3%  | **-4%** | -7%  | -9%  | -20% |

### Controlling for code layout influence

In order to control the measurement bias due to code layout, we ran
the same benchmark with different combinations of compiler and
compiler options, for instance:
* `gcc -Wa,-mbranches-within-32B`
* `gcc -Wa,-mbranches-within-32B -march=skylake`
* `clang -mbranches-within-32B-boundaries`
* `clang -mbranches-within-32B-boundaries -march=skylake`
* `gcc -Wa,-mbranches-within-32B -mtune=skylake`
* `gcc -Wa,-mbranches-within-32B -falign-jumps=16 -falign-loops=16 -falign-functions=16`

All give different code layouts in the function `do_some_marking` (and
outside of it).

With different compilers and compiler options, we also checked whether
the code was strongly affected by alignment by adding from 1 to 20
bytes of padding NOPs at the beginning of the function
`do_some_marking`. The code of the function is not strongly affected
by alignment. There could, rarely, be a difference of up to 1% with
manual padding compared to no padding (in the benchmark with 5% skip
ratio). The differences observed with the page table were more than
twice the maximum observed fluctuation due to changing alignment by
hand.

By using `clang` and `gcc -march=skylake` (but not `-mtune=skylake`),
we saw the smallest difference between nnp and the page table, but it
still resulted in a measured speedup of the page table (at 0% skip
ratio: without the benefits of skipping static data). No compiler or
compiler option showed the nnp variant being faster.

Thus, we are unable to explain the speedup (even at 0% skip ratio)
solely with code layout effects. In addition, the extra gain at higher
skip ratio was unaffected by alignment. (This is a qualitative method,
it does not say by _how much_ the observed speedup results is higher
or lower due to irrelevant micro-architetural effects.)

### A possible explanation for the speedup at 0% skip ratio

The marking loop optimised for memory-level parallelism iterates on
the values `v` in a block with a test of the following form:
```c
if (is_block_and_in_heap(v)) {
    prefetch(v);
    enqueue(v);
}
```

With a page table, the test `is_block_and_in_heap(v)` is `Is_block(v)
&& Is_in_heap(v)`. In “no-naked-pointers” mode, the test
`is_block_and_in_heap(v)` amounts to `Is_block(v) && !Is_young(v)`,
but the implementation in `mark-prefetching` uses a single range test
using a rotation trick to eliminate the `Is_block` test.

The `Is_block` test is hard to predict in general (in some of our
synthetic benchmarks, and also in real-world programs as per our
measurements), whereas the `Is_young` test is always very predictable
given how rare pointers from the major heap to the minor heap are.
Thus, taking branch predition into account, the simpler `Is_block`
test lets the prediction be corrected earlier (by a couple of cycles,
which is consistent with the observed speedup), whereas branch
prediction makes the second test come for free.

We have observed that a branch that tests the two conditions
separately shows the same speedup as our page table
(`mark-prefetching-412-stats-is_not_young+nnp`).

### Non-random heap (08/2022)

A new variable has been introduced, to control the degree of
randomness of the heap layout. With the new test `norandom`, the heap
is allocated without any randomness. This maximises the memory
locality of the heap during traversal by GC for marking. The effect is
to divide the amount of cycles spent stalling (despite prefetching)
due to cache misses by ~5, bringing its proportion to the total amount
of cycles from 22% to 7%.

In the original benchmark, giving the heap a random layout was meant
to try to exhaust the MLP resources of the CPU. The goal of this new
variable is to test the hypothesis that an artifial amount of stalls
might hide the cost of page table checks.

We reproduced the previous results without randomness in the heap, and
found the same conclusions regarding the performance of the page table
implementations. As for the benefit of huge pages, it is much lesser
with the artificially memory-local heap than with the artifically
random heap (as it can be expected).

The results were obtained with CPUs AMD Ryzen 5850U and Intel
i7-1185G7, which also means that the results have been reproduced with
these newer CPUs.

## Conclusion

### Page table vs. no-naked-pointers

Contrary to the expectation that a page table would have an
extravagant cost, our page table consistently outperforms slightly the
`mark-prefetching` branch in no-naked-pointers mode in our synthetic
benchmark. This is further improved in this synthetic benchmark by
taking the skipping of static data into account (however this speedup
does not account for the cost of new branch mispredictions). Static
data was found to be visited fairly frequently during marking in some
real-world workloads (5% of visited words on average, with spikes at
40% during some marking slices).

In results with real-world benchmarks with large heaps (in the
directory `real_perfs2`), we could distinguish a speedup with the page
table probably due to skipping of static data (between 0-5%; this kind
of results do not let us evaluate by how much precisely). See
Limitations below. (We have also measured the impact of
re-implementing lazy short-cutting, which was removed with the new
prefetching marking loop, and we found no significant cost in the same
real-world benchmarks.)

The current page table scales poorly to large heaps, and to multicore.
The new page table scales to large heaps both in theory and practice.
As for scaling to multicore, the figures that we have reported are for
an implementation that already supports parallel accesses to the page
table, using a monotonicity assumption which is realistic for
use-cases of the page table (see the ML workshop paper). We reasoned
that this single-core performance is representative of the multi-core
performance by a theoretical argument.

### Transparent huge pages

Transparent huge pages bring comparable improvements by using jemalloc
or our custom heap chunk allocator (from about 10% in our synthetic
benchmark with a random heap, to unmeasurable with a non-random
one).

A built-in allocator has further advantages, such as using THP only
for memory allocations well-suited for huge pages: the major and minor
heaps are indeed especially well suited, since they are allocated and
deallocated in large chunks. In contrast, the option `thp:always` of
jemalloc concerns all allocations indiscriminately. This can be
problematic if the program is linked with non-OCaml libraries that are
not well-suited to huge pages.

This friendliness to huge pages is due in particular to memory
reclamation being performed via compaction, which frees up memory in
large chunks. Regarding multicore, while it is possible to implement
memory reclamation without compaction, alternatives would not be as
friendly to huge page allocation and would encounter known issues of
huge pages regarding memory reclamation. Concretely, with huge pages,
a single live value can retain 2MB of memory instead of 4KB, or force
the splitting of a huge page into small pages, which should usually be
avoided.

### Limitations

* We used a synthetic benchmark that exerts either a lot of cache
  misses, or very few. The synthetic benchmark also fails to reproduce
  the branch prediction behaviour, which can affect how much we can
  gain from skipping static data.

* Better instrumentation is needed for real-world benchmarks, given
  that the data is noisy and hard to exploit. Some raw results on
  OCaml and Coq workloads are available in the `real_perf` directory.
  We observed that nnp and the page table have similar performance,
  with an indistinguishable difference in total running time. However
  it was unclear whether the benefits of skipping static data
  translate into practice, which may be due to the costs of branch
  mispredictions this creates. The `real_perf2` directory contains raw
  results for a different implementation (and with a more recent CPU),
  with which we could distinguish a speedup due to the page table.
  This implementation relies on dependency ordering on Arm and does
  not implement the tainting of the page table (this more recent
  implementation is fine, but this is not the one that has been
  discussed in this document).

* The synthetic benchmark only measures the time for doing a major GC,
  while huge pages are likely to benefit the whole program.
  Preliminary results on a real-world workload have been encouraging
  (e.g. https://github.com/coq/coq/pull/14254#issuecomment-834432586).

* We only tested OCaml and Coq regarding immediate and static data
  usage, on intensive and real-world workloads. It would be nice to
  get statistics on more programs.

* We implemented our designs for POSIX systems, with THP being
  specific to Linux. Virtual address space reservation works a bit
  differently in Windows, but our designs can also be implemented
  there. A lot of knowledge on porting to other platforms can be found
  in the well-documented source code of the Go runtime.

* It is unclear that the benefits of the page protection tricks for
  the first level of the page table are worth its complexity. It turns
  out to be a secondary aspect not needed for efficiency. It allows
  the use of out-of-heap pointers without their pages having been
  registered with the page table in advance (e.g. for
  backwards-compatibility). This feature could instead be implemented
  as a backwards-compatible mode, or as a 3rd-party library. For other
  uses of out-of-heap pointers, we could ask that pages are registered
  in advance. Or the whole page table (4MB with 48-bit address space)
  could simply be allocated from the start. On some setups such as
  Linux with overcommitting, this does not consume memory except for
  pages that have been touched.

### Thanks

Thank you to Stephen Dolan for his original PR and benchmark which
were a sound playground for this experiment, as well as his patient
explanations, help and advice (bad performance in 32-bit in an earlier
version, understanding the results, controlling for layout effects,
effect of stalls). Thank you to Pierre-Marie Pédrot for early runs of
running the Coq benchmark suite on some of my branches. (Any error or
opinion left in this document is my own.)

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#define __USE_GNU
#include <sys/mman.h>
#include <assert.h>

// gcc -O3 mremap.c && perl -MTime::HiRes -e '$t1=Time::HiRes::time;system(q[TEST_MREMAP_DOWN=1 ./a.out]);$t2=Time::HiRes::time;printf qq[%f µs\n],($t2-$t1)*1_000_000/5;'
// allocating 1024 MB
// mremap_down
// success!
// 532101.583481 µs

// gcc -O3 mremap.c && perl -MTime::HiRes -e '$t1=Time::HiRes::time;system(q[TEST_MREMAP=1 ./a.out]);$t2=Time::HiRes::time;printf qq[%f µs\n],($t2-$t1)*1_000_000/5;'
// allocating 1024 MB
// mremap
// success!
// 1007746.601105 µs

// gcc -O3 mremap.c && perl -MTime::HiRes -e '$t1=Time::HiRes::time;system(q[TEST_MMAP=1 ./a.out]);$t2=Time::HiRes::time;printf qq[%f µs\n],($t2-$t1)*1_000_000/5;'
// allocating 1024 MB
// mmap
// success!
// 1002819.967270 µs

// gcc -O3 mremap.c && perl -MTime::HiRes -e '$t1=Time::HiRes::time;system(q[TEST_MEMCPY=1 ./a.out]);$t2=Time::HiRes::time;printf qq[%f µs\n],($t2-$t1)*1_000_000/5;'
// allocating 1024 MB
// memcpy
// success!
// 2419597.387314 µs

// gcc -O3 mremap.c && perl -MTime::HiRes -e '$t1=Time::HiRes::time;system(q[TEST_COPY=1 ./a.out]);$t2=Time::HiRes::time;printf qq[%f µs\n],($t2-$t1)*1_000_000/5;'
// allocating 1024 MB
// copy
// success!
// 2162356.805801 µs

// gcc -O3 mremap.c && perl -MTime::HiRes -e '$t1=Time::HiRes::time;system(q[TEST_REALLOC=1 ./a.out]);$t2=Time::HiRes::time;printf qq[%f µs\n],($t2-$t1)*1_000_000/5;'
// allocating 1024 MB
// realloc
// success!
// 1008677.816391 µs

// TODO: test THP

#define NUM 5
#define PAGES_LOG 18
#define PAGES (1 << PAGES_LOG)
#define PAGE_SIZE 4096
#define SIZE (PAGES * PAGE_SIZE)

int check(char *m)
{
  int ret = 1;
  int i;
  for (i = 0; i < PAGES; i++) {
    ret = ret && (m[i * PAGE_SIZE] == 1);
  }
  return ret;
}

void set(char *m, size_t a, size_t b)
{
  int i;
  for (i = a; i < b; i += PAGE_SIZE) {
    m[i] = 1;
  }
}

// All tests are duplicated to have some interference between
// allocations.

// Stack needs to grow down.
// Fastest speed achieved thanks to MEM_POPULATE
char *mremap_down(char *p, size_t old_size, size_t new_size)
{
  size_t ext_size = new_size - old_size;
  char *p2, *p3, *p4;
  if (ext_size == 0) return p;
  assert(old_size < new_size);
  // Try to expand directly below
  p2 = mmap(p - ext_size, ext_size, PROT_READ | PROT_WRITE,
            MAP_POPULATE | MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
            -1, 0);
  if (p2 != MAP_FAILED) return p2;
  // This failed, so try to remap
  // Reserve a new map
  p2 = mmap(0, new_size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (p2 == MAP_FAILED) return MAP_FAILED;
  // Commit the extension
  p3 = mmap(p2, ext_size, PROT_READ | PROT_WRITE,
            MAP_POPULATE | MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED,
            -1, 0);
  if (p3 == MAP_FAILED) goto out_err;
  // Now try to move the old mapping.
  // One has to move the old mappings one by one.
  p4 = p2 + ext_size;
  while (old_size > PAGE_SIZE) {
    old_size >>= 1;
    p4 = mremap(p, old_size, old_size, MREMAP_MAYMOVE | MREMAP_FIXED, p4);
    if (p4 == MAP_FAILED) goto out_err;
    p += old_size;
    p4 += old_size;
  }
  assert(old_size == PAGE_SIZE);
  p4 = mremap(p, old_size, old_size, MREMAP_MAYMOVE | MREMAP_FIXED, p4);
  if (p4 == MAP_FAILED) goto out_err;
  return p2;
out_err:
  perror("mremap_down");
  munmap(p2, new_size);
  return MAP_FAILED;
}

int alloc_mremap(int grows_down)
{
  int i;
  char *m1 = NULL;
  char *m2 = NULL;
  size_t size = PAGE_SIZE;
  int ret = 1;
  m1 = (char *)mmap(0, size, PROT_READ | PROT_WRITE, MAP_POPULATE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  m2 = (char *)mmap(0, size, PROT_READ | PROT_WRITE, MAP_POPULATE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (m1 == MAP_FAILED || m2 == MAP_FAILED) printf("mmap failed!\n");
  m1[0] = 1;
  m2[0] = 1;
  for (i=0; i<PAGES_LOG; i++) {
    // mremap(void *old_address, size_t old_size, size_t new_size,int flags, /* void *new_address */);
    if (grows_down) {
      m1 = (char *)mremap_down(m1, size, 2 * size);
      m2 = (char *)mremap_down(m2, size, 2 * size);
      if (m1 == MAP_FAILED || m2 == MAP_FAILED) printf("mremap_down failed!\n");
      set(m1, 0, size);
      set(m2, 0, size);
    } else {
      m1 = (char *)mremap(m1, size, 2 * size, MREMAP_MAYMOVE);
      m2 = (char *)mremap(m2, size, 2 * size, MREMAP_MAYMOVE);
      if (m1 == MAP_FAILED || m2 == MAP_FAILED) printf("mremap failed!\n");
      set(m1, size, 2 * size);
      set(m2, size, 2 * size);
    }
    size *= 2;
  }
  assert(size == SIZE);
  ret = check(m1) && check(m2);
  munmap(m1, size);
  munmap(m2, size);
  return ret;
}

void commit(char *p, size_t size)
{
  if (-1 == mprotect(p, size, PROT_READ | PROT_WRITE)) {
    perror("commit");
    return;
  }
  madvise(p, size, MADV_WILLNEED);
}

int alloc_mmap()
{
  int i;
  char *m1 = NULL;
  char *m1_top = NULL;
  char *m2 = NULL;
  char *m2_top = NULL;
  size_t size = PAGE_SIZE;
  int ret = 1;
  m1 = (char *)mmap(0, SIZE, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  m2 = (char *)mmap(0, SIZE, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (m1 == MAP_FAILED || m2 == MAP_FAILED) printf("mmap failed!\n");
  m1_top = m1 + SIZE - size;
  m2_top = m2 + SIZE - size;
  commit(m1_top, size);
  commit(m2_top, size);
  m1_top[0] = 1;
  m2_top[0] = 1;
  for (i=0; i<PAGES_LOG; i++) {
    m1_top -= size;
    m2_top -= size;
    commit(m1_top, size);
    commit(m2_top, size);
    set(m1_top, 0, size);
    set(m2_top, 0, size);
    size *= 2;
  }
  assert(size == SIZE);
  assert(m1_top == m1);
  ret = check(m1) && check(m2);
  munmap(m1, size);
  munmap(m2, size);
  return ret;
}

// Growing up or down is the same
int alloc_copy(int do_memcpy)
{
  int i;
  char *m1 = NULL;
  char *m2 = NULL;
  size_t size = PAGE_SIZE;
  int ret = 1;
  m1 = (char *)malloc(size);
  m2 = (char *)malloc(size);
  if (m1 == NULL || m2 == NULL) printf("malloc failed!\n");
  m1[0] = 1;
  m2[0] = 1;
  for (i=0; i<PAGES_LOG; i++) {
    char *m12 = malloc(2 * size);
    char *m22 = malloc(2 * size);
    if (m12 == NULL || m22 == NULL) printf("malloc failed!\n");
    if (do_memcpy) {
      //memcpy(void *dest, const void *src, size_t n);
      memcpy(m12, m1, size);
      memcpy(m22, m2, size);
    } else {
      size_t j;
      for (j = 0; j < size; j++) {
        m12[j] = m1[j];
      }
      for (j = 0; j < size; j++) {
        m22[j] = m2[j];
      }
    }
    free(m1);
    free(m2);
    m1 = m12;
    m2 = m22;
    set(m1, size, 2 * size);
    set(m2, size, 2 * size);
    size *= 2;
  }
  assert(size == SIZE);
  ret = check(m1) && check(m2);
  free(m1);
  free(m2);
  return ret;
}

// Note that this does not grow down
int alloc_realloc()
{
  int i;
  char *m1 = NULL;
  char *m2 = NULL;
  size_t size = PAGE_SIZE;
  int ret = 1;
  m1 = (char *)malloc(size);
  m2 = (char *)malloc(size);
  if ((m1 == NULL) || (m2 == NULL)) printf("malloc failed!\n");
  m1[0] = 1;
  m2[0] = 1;
  for (i=0; i<PAGES_LOG; i++) {
    m1 = (char *)realloc(m1, 2 * size);
    m2 = (char *)realloc(m2, 2 * size);
    if ((m1 == NULL) || (m2 == NULL)) printf("realloc failed!\n");
    set(m1, size, 2 * size);
    set(m2, size, 2 * size);
    size *= 2;
  }
  assert(size == SIZE);
  ret = check(m1) && check(m2);
  free(m1);
  free(m2);
  return ret;
}

int main()
{
  int i;
  int ret;

  printf("allocating %u MB\n", (unsigned)(SIZE / 1024 / 1024));
  if (getenv("TEST_MREMAP")) {
    printf("mremap\n");
    for (i=0; i<NUM; i++) {
      ret = alloc_mremap(0);
    }
  }
  else if (getenv("TEST_MREMAP_DOWN")) {
    printf("mremap_down\n");
    for (i=0; i<NUM; i++) {
      ret = alloc_mremap(1);
    }
  }
  else if (getenv("TEST_MMAP")) {
    printf("mmap\n");
    for (i=0; i<NUM; i++) {
      ret = alloc_mmap(1);
    }
  }
  else if (getenv("TEST_MEMCPY")) {
    printf("memcpy\n");
    for (i=0; i<NUM; i++) {
      ret = alloc_copy(1);
    }
  } else if (getenv("TEST_REALLOC")) {
    printf("realloc\n");
    for (i=0; i<NUM; i++) {
      ret = alloc_realloc();
    }
  } else {
    printf("copy\n");
    for (i=0; i<NUM; i++) {
      ret = alloc_copy(0);
    }
  }
  if (ret) printf("success!\n");
  else printf("failure!\n");
  return 0;
}

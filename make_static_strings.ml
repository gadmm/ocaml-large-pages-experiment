let num = 8000
let num_per_func = 100
let () = assert (0 = num mod num_per_func)
let num_func = num / num_per_func
let print = Printf.printf

let () =
  print "let num_strings : int = %n\n\n" num;
  for i = 0 to num - 1 do
    print "let s%n : string = \"Here is a string of one cache line: ................%n\"\n" i i
  done;
  for i = 0 to num_func - 1 do
    print "\nlet nth_string%n (n : int) : string = match n mod %n with\n" i num_per_func;
    for j = 0 to num_per_func - 1 do
      print "| %n -> s%n\n" j (i * num_per_func + j)
    done;
    print "| _ -> failwith \"invalid argument\"\n\n";
  done;
  print "\nlet nth_func (n : int) : string = match n / %n with\n" num_per_func;
  for i = 0 to num_func - 1 do
    print "| %n -> nth_string%n n\n" i i
  done;
  print "| _ -> failwith \"invalid argument\"\n\n";
  print "let string_of_index n = nth_func (n mod num_strings)\n"
